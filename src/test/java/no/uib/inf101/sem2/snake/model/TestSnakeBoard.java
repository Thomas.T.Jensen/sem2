package no.uib.inf101.sem2.snake.model;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import no.uib.inf101.sem2.grid.CellPosition;

public class TestSnakeBoard {
    private SnakeBoard board;
    private final int rows = 10;
    private final int cols = 10;

    @BeforeEach
    public void setUp() {
        board = new SnakeBoard(rows, cols);
    }

    @Test
    public void testConstructor() {
        assertEquals(rows, board.rows());
        assertEquals(cols, board.cols());
        for (int row = 0; row < rows; row++) {
            for (int col = 0; col < cols; col++) {
                assertEquals('-', board.get(new CellPosition(row, col)));
            }
        }
    }

    @Test
    public void testToString() {
        // Place snake head, body, and apple on the board
        board.set(new CellPosition(5, 5), 'H');
        board.set(new CellPosition(5, 6), 'S');
        board.set(new CellPosition(5, 7), 'S');
        board.set(new CellPosition(7, 7), 'A');

        // Build the expected board string representation
        String expected = "- - - - - - - - - -" +
                "\n- - - - - - - - - -" +
                "\n- - - - - - - - - -" +
                "\n- - - - - - - - - -" +
                "\n- - - - - - - - - -" +
                "\n- - - - - H S S - -" +
                "\n- - - - - - - - - -" +
                "\n- - - - - - - A - -" +
                "\n- - - - - - - - - -" +
                "\n- - - - - - - - - -";

        String actual = board.toString();
        assertEquals(expected, actual);
    }

}
