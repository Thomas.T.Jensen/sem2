package no.uib.inf101.sem2.snake.model;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import no.uib.inf101.sem2.grid.CellPosition;
import no.uib.inf101.sem2.grid.GridCell;
import no.uib.inf101.sem2.snake.model.Snake.Snake;

public class TestSnake {

    private Snake snake;
    private char bodySymbol = 'o';
    private char headSymbol = '@';
    private int snakeSize = 5;
    private CellPosition headPos = new CellPosition(5, 5);

    @BeforeEach // https://stackoverflow.com/questions/63819472/how-can-i-use-the-beforeeach-method-for-testing-in-java
    void setUp() {
        snake = Snake.createSnake(bodySymbol, headSymbol, headPos, snakeSize);
    }

    @Test
    public void testHashCodeAndEquals() {
        Snake s1 = Snake.createSnake('b', 'h', new CellPosition(5, 5), 3);
        Snake s2 = Snake.createSnake('b', 'h', new CellPosition(5, 5), 3);
        Snake s3 = Snake.createSnake('b', 'h', new CellPosition(5, 6), 3);
        Snake s4 = Snake.createSnake('c', 'h', new CellPosition(5, 5), 3);

        assertEquals(s1, s2);
        assertEquals(s1.hashCode(), s2.hashCode());
        assertNotEquals(s1, s3);
        assertNotEquals(s1, s4);
    }

    @Test
    public void snakeIteration() {
        // Create a snake with body length 3
        Snake snake = Snake.createSnake('b', 'h', new CellPosition(5, 5), 3);

        // Collect the iterated objects
        List<GridCell<Character>> objs = new ArrayList<>();
        for (GridCell<Character> gc : snake) {
            objs.add(gc);
        }

        // Check that we got the expected GridCell objects
        assertEquals(3, objs.size());
        assertTrue(objs.contains(new GridCell<>(new CellPosition(5, 5), 'h')));
        assertTrue(objs.contains(new GridCell<>(new CellPosition(5, 4), 'b')));
        assertTrue(objs.contains(new GridCell<>(new CellPosition(5, 3), 'b')));
    }

    @Test
    void withHeadAt() {
        CellPosition newHeadPos = new CellPosition(5, 6);
        Snake newSnake = snake.withHeadAt(newHeadPos);

        assertNotEquals(snake, newSnake);
        assertEquals(newHeadPos, newSnake.getHeadPos());
    }

    @Test
    public void shiftedBy() {
        Snake newSnake = snake.shiftedBy(0, 1);

        assertNotEquals(snake, newSnake);
        assertEquals(new CellPosition(headPos.row(), headPos.col() + 1), newSnake.getHeadPos());
    }

}