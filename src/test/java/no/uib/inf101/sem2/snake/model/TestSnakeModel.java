package no.uib.inf101.sem2.snake.model;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import no.uib.inf101.sem2.snake.controller.DifficultyLevel;

public class TestSnakeModel {
    private SnakeModel snakeModel;
    private SnakeBoard board;

    @BeforeEach // https://stackoverflow.com/questions/63819472/how-can-i-use-the-beforeeach-method-for-testing-in-java
    public void setUp() {
        board = new SnakeBoard(10, 10);
        snakeModel = new SnakeModel(board);
    }

    @Test
    public void testInitialGameState() {
        assertEquals(GameState.HOME, snakeModel.getGameState());
    }

    @Test
    public void testMoveSnake() {
        assertTrue(snakeModel.moveSnake(0, 1)); // Move right
        assertFalse(snakeModel.moveSnake(0, -1)); // Move left, which is not allowed since it would collide with the
                                                  // snake's body
    }

    @Test
    public void testInitialScore() {
        assertEquals(0, snakeModel.getScore());
    }

    @Test
    public void testInitialHighScore() {
        assertEquals(0, snakeModel.getHighScore());
    }

    @Test
    public void testResetHome() {
        snakeModel.resetHome();
        assertEquals(GameState.HOME, snakeModel.getGameState());
        assertEquals(0, snakeModel.getScore());
    }

    @Test
    public void testReset() {
        snakeModel.reset();
        assertEquals(GameState.ACTIVE, snakeModel.getGameState());
        assertEquals(0, snakeModel.getScore());
    }

    @Test
    public void testSetGameState() {
        snakeModel.setGameState(GameState.PAUSED);
        assertEquals(GameState.PAUSED, snakeModel.getGameState());
    }

    @Test
    public void testUpdateHighScore() {
        snakeModel.setGameState(GameState.GAME_OVER);
        assertEquals(0, snakeModel.getHighScore());
        snakeModel.moveSnake(0, 1);
        snakeModel.setGameState(GameState.GAME_OVER);
        assertEquals(0, snakeModel.getHighScore());
    }

    @Test
    public void testSetInitialDirection() {
        snakeModel.setInitialDirection();
        assertEquals(Direction.RIGHT, snakeModel.getCurrentDirection());
    }

    @Test
    public void testSetDifficultyLevel() {
        snakeModel.setDifficultyLevel(DifficultyLevel.HARD);
        assertEquals(DifficultyLevel.HARD, snakeModel.getDifficultyLevel());
    }

    @Test
    public void testMoveOutOfBoard() {
        SnakeBoard board = new SnakeBoard(20, 10);
        SnakeModel model = new SnakeModel(board);

        model.moveSnake(20, 0);
        assertFalse(model.getGameState() == GameState.ACTIVE);
    }

    @Test
    public void testMoveToOccupiedTile() {
        SnakeBoard board = new SnakeBoard(20, 10);
        SnakeModel model = new SnakeModel(board);

        assertFalse(model.moveSnake(0, -1));
    }

}
