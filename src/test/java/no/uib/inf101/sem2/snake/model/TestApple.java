package no.uib.inf101.sem2.snake.model;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.Set;
import java.util.stream.Collectors;

import org.junit.jupiter.api.Test;

import no.uib.inf101.sem2.grid.CellPosition;
import no.uib.inf101.sem2.snake.model.Apple.Apple;
import no.uib.inf101.sem2.snake.model.Apple.RandomApple;
import no.uib.inf101.sem2.snake.model.Snake.Snake;

public class TestApple {

    @Test
    public void testGeneratedApplePosition() {
        int boardRows = 20;
        int boardCols = 10;
        RandomApple randomApple = new RandomApple(boardRows, boardCols);
        Snake snake = Snake.createSnake('S', 'H', new CellPosition(5, 5), 3);

        for (int i = 0; i < 100; i++) {
            Apple apple = randomApple.getNext(snake);
            assertNotNull(apple);

            CellPosition applePos = apple.getPosition();
            assertNotNull(applePos);

            // Check if the apple is within the board boundaries
            assertTrue(applePos.row() >= 0 && applePos.row() < boardRows);
            assertTrue(applePos.col() >= 0 && applePos.col() < boardCols);

            // Check if the apple position is not occupied by the snake
            Set<CellPosition> snakeBody = snake.getBody().stream().collect(Collectors.toSet());
            assertFalse(snakeBody.contains(applePos));
        }
    }

}
