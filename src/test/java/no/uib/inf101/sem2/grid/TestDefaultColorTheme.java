package no.uib.inf101.sem2.grid;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.awt.Color;

import org.junit.jupiter.api.Test;

import no.uib.inf101.sem2.snake.view.ColorTheme;
import no.uib.inf101.sem2.snake.view.DefaultColorTheme;

public class TestDefaultColorTheme {
    @Test
    public void sanityTestDefaultColorTheme() {
        ColorTheme colors = new DefaultColorTheme();
        assertEquals(Color.LIGHT_GRAY, colors.getBackgroundColor());

    }

}
