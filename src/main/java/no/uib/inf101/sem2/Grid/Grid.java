package no.uib.inf101.sem2.grid;

import java.util.ArrayList;
import java.util.Iterator;

//The whole Grid Class is made with help from ColorGrid Lab4.

public class Grid<E> implements IGrid<E> {
    // Define instance variables
    private final int rows;
    private final int cols;
    protected ArrayList<ArrayList<E>> grid;

    // Constructor with two arguments, rows and cols
    public Grid(int rows, int cols) {
        // Call the other constructor with three arguments and pass null as the default
        // value
        this(rows, cols, null);
    }

    /**
     * Constructs a new Grid object with the specified number of rows and columns.
     * All elements in the grid are initialized to null.
     *
     * @param rows the number of rows in the grid
     * @param cols the number of columns in the grid
     */
    public Grid(int rows, int cols, E defaultValue) {
        // Initialize instance variables
        this.rows = rows;
        this.cols = cols;
        this.grid = new ArrayList<>();
        // Set the default value for each cell in the grid
        for (int i = 0; i < rows; i++) {
            ArrayList<E> row = new ArrayList<>();
            for (int j = 0; j < cols; j++) {
                row.add(defaultValue);
            }
            grid.add(row);
        }
    }

    @Override
    public int rows() {
        return rows;
    }

    @Override
    public int cols() {
        return cols;
    }

    @Override
    public E get(CellPosition position) {
        // Throw an exception if the position is not on the grid
        if (!positionIsOnGrid(position)) {
            throw new IndexOutOfBoundsException();
        }
        // Return the value at the specified position in the grid
        return grid.get(position.row()).get(position.col());
    }

    @Override
    public void set(CellPosition position, E value) {
        // Throw an exception if the position is not on the grid
        if (!positionIsOnGrid(position)) {
            throw new IndexOutOfBoundsException();
        }
        // Set the value at the specified position in the grid
        grid.get(position.row()).set(position.col(), value);
    }

    @Override
    public boolean positionIsOnGrid(CellPosition position) {
        // Check if the row and column indices are within the bounds of the grid
        return position.row() >= 0 && position.row() < rows
                && position.col() >= 0 && position.col() < cols;
    }

    @Override
    public Iterator<GridCell<E>> iterator() {
        // create an ArrayList to store GridCell objects
        ArrayList<GridCell<E>> cells = new ArrayList<GridCell<E>>();
        // iterate over all rows and columns
        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < cols; j++) {
                // create a CellPosition object for the current row and column
                CellPosition pos = new CellPosition(i, j);
                // get the value at the current CellPosition
                E value = get(pos);
                // add the new GridCell object to the ArrayList
                cells.add(new GridCell<E>(pos, value));
            }
        }
        // return the Iterator associated with the ArrayList of GridCell objects
        return cells.iterator();
    }

}
