package no.uib.inf101.sem2.grid;

public record CellPosition(int row, int col) {

    // The record class automatically generates the following:
    // - Constructor with parameters for all fields (in this case, row and col)
    // - Getter methods for all fields
    // - hashCode() and equals() methods that compare the values of all fields
    // - A toString() method that returns a string representation of the object

    // No additional code is needed, as the generated code is sufficient.

}
