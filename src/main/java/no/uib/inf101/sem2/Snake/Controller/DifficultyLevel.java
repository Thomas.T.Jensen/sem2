package no.uib.inf101.sem2.snake.controller;

// DifficultyLevel is an enum that represents the different difficulty levels in the Snake game.
public enum DifficultyLevel {

    EASY(200),

    MEDIUM(100),

    HARD(50),

    TIMED(80); // add a new difficulty level for the timed mode

    private int delayTime;

    /**
     * Constructs a new `DifficultyLevel` with the specified time delay.
     * 
     * @param delayTime the time delay, in milliseconds, for this difficulty level
     */
    DifficultyLevel(int delayTime) {
        this.delayTime = delayTime;
    }

    /**
     * Returns the time delay, in milliseconds, for this difficulty level.
     * 
     * @return the time delay, in milliseconds, for this difficulty level
     */
    public int getDelayTime() {
        return delayTime;
    }
}
