package no.uib.inf101.sem2.snake.model.Apple;

import java.util.Random;
import java.util.Set;
import java.util.stream.Collectors;

import no.uib.inf101.sem2.grid.CellPosition;
import no.uib.inf101.sem2.snake.model.Snake.Snake;

public class RandomApple implements AppleFactory {
    private final int boardRows;
    private final int boardCols;
    private final Random random;

    public RandomApple(int boardRows, int boardCols) {
        this.boardRows = boardRows;
        this.boardCols = boardCols;
        random = new Random();
    }

    /**
     * Generates a random position on the game board that is not overlapping with
     * the Snake body.
     * 
     * @param snakeBody the set of CellPositions occupied by the Snake body
     * @return a new CellPosition for the apple
     */
    public CellPosition getRandomPosition(Set<CellPosition> snakeBody) {
        CellPosition randomPosition;
        do {
            int row = random.nextInt(boardRows);
            int col = random.nextInt(boardCols);
            randomPosition = new CellPosition(row, col);
        } while (snakeBody.contains(randomPosition));
        return randomPosition;
    }

    @Override
    public Apple getNext(Snake snake) {
        Set<CellPosition> snakeBody = snake.getBody().stream().collect(Collectors.toSet());
        return new Apple('A', getRandomPosition(snakeBody));
    }
}
