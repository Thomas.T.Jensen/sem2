package no.uib.inf101.sem2.snake.view;

import java.awt.Color;

public class DefaultColorTheme implements ColorTheme {

    @Override
    public Color getBackgroundColor() {
        return Color.LIGHT_GRAY;
    }

}
