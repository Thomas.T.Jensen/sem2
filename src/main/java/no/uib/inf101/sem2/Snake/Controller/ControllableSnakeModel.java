package no.uib.inf101.sem2.snake.controller;

import no.uib.inf101.sem2.snake.model.GameState;

// ControllableSnakeModel is an interface that represents a controllable Snake model.
public interface ControllableSnakeModel {

    /**
     * Attempts to move the snake by the specified delta values.
     * 
     * @param deltaRow the change in row position
     * @param deltaCol the change in column position
     * @return `true` if the snake was able to move successfully, `false` otherwise
     */
    public Boolean moveSnake(int deltaRow, int deltaCol);

    /**
     * Sets the game state for the Snake game.
     * 
     * @param state the new game state
     */

    public void setGameState(GameState state);

    /**
     * Sets the difficulty level for the Snake game.
     * 
     * @param difficultyLevel the new difficulty level
     */
    public void setDifficultyLevel(DifficultyLevel difficultyLevel);

    /**
     * Updates the high score achieved in the Snake game.
     */
    public void updateHighScore();

}
