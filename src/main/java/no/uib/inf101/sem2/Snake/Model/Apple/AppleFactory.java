package no.uib.inf101.sem2.snake.model.Apple;

import no.uib.inf101.sem2.snake.model.Snake.Snake;

// AppleFactory is an interface that represents a factory for creating apples in the Snake game.
public interface AppleFactory {

    /**
     * Returns the next apple that should be placed on the board, given the current
     * snake.
     * 
     * @param snake the current snake on the board
     * @return the next apple that should be placed on the board
     */
    Apple getNext(Snake snake);
}
