package no.uib.inf101.sem2.snake.model;

import no.uib.inf101.sem2.grid.CellPosition;
import no.uib.inf101.sem2.grid.Grid;

//SnakeBoard is a class that extends the `Grid` class and represents the game board for the Snake game.
public class SnakeBoard extends Grid<Character> {

    /**
     * Constructs a new SnakeBoard with the specified number of rows and columns and
     * initializes all elements to the specified character.
     * 
     * @param rows the number of rows
     * @param cols the number of columns
     */
    public SnakeBoard(int rows, int cols) {
        super(rows, cols, '-');

    }

    /**
     * Returns a string representation of the SnakeBoard. The string contains each
     * element of the SnakeBoard, separated by spaces for each row, and each row
     * separated by a newline character.
     * 
     * @return a string representation of the SnakeBoard
     */
    public String toString() {
        StringBuilder sb = new StringBuilder();
        for (int row = 0; row < rows(); row++) {
            for (int col = 0; col < cols(); col++) {
                sb.append(get(new CellPosition(row, col)));
                if (col < cols() - 1) {
                    sb.append(" ");
                }
            }
            if (row < rows() - 1) {
                sb.append("\n");
            }
        }
        return sb.toString();
    }

}