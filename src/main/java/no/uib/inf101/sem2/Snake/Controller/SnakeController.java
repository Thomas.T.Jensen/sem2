package no.uib.inf101.sem2.snake.controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import javax.swing.Timer;

import no.uib.inf101.sem2.snake.model.Direction;
import no.uib.inf101.sem2.snake.model.GameState;
import no.uib.inf101.sem2.snake.model.SnakeModel;
import no.uib.inf101.sem2.snake.view.SnakeView;

public class SnakeController implements KeyListener, ActionListener {
    private SnakeModel model;
    private SnakeView view;
    private Timer timer;
    private DifficultyLevel difficultyLevel = DifficultyLevel.MEDIUM;
    private int gameTime = 400; // 400 seconds for the timed mode
    private int timeRemaining = gameTime; // initialize with the total time
    private int currentDeltaRow = 0;
    private int currentDeltaCol = 1;

    public SnakeController(SnakeModel model, SnakeView view) {
        this.model = model;
        this.view = view;

        view.addKeyListener(this);
        view.setFocusable(true);

        timer = new Timer(difficultyLevel.getDelayTime(), this);
        this.timer.setInitialDelay(0); // start the timer immediately
        this.timer.start();

        if (difficultyLevel == DifficultyLevel.TIMED) {
            // Create a new timer for the timed mode countdown
            Timer countdownTimer = new Timer(1000, this); // 1 second interval
            countdownTimer.start();
        }
    }

    /**
     * Sets the delay for the `Timer`.
     * 
     * @param delay the new delay value
     */
    public void setTimerDelay(int delay) {
        timer.setDelay(delay);
    }

    /**
     * Pauses or resumes the game by stopping or starting the `Timer`.
     */
    public void pauseGame() {
        if (timer.isRunning()) {
            timer.stop();
        } else {
            timer.start();
        }
    }

    @Override
    public void keyPressed(KeyEvent e) {
        if (model.getGameState() == GameState.HOME) {
            switch (e.getKeyCode()) {
                case KeyEvent.VK_1:
                    model.setDifficultyLevel(DifficultyLevel.EASY);
                    setTimerDelay(model.getDifficultyLevel().getDelayTime());
                    model.setGameState(GameState.ACTIVE);
                    break;
                case KeyEvent.VK_2:
                    model.setDifficultyLevel(DifficultyLevel.MEDIUM);
                    setTimerDelay(model.getDifficultyLevel().getDelayTime());
                    model.setGameState(GameState.ACTIVE);
                    break;
                case KeyEvent.VK_3:
                    model.setDifficultyLevel(DifficultyLevel.HARD);
                    setTimerDelay(model.getDifficultyLevel().getDelayTime());
                    model.setGameState(GameState.ACTIVE);
                    break;
                case KeyEvent.VK_4:
                    model.setDifficultyLevel(DifficultyLevel.TIMED);
                    setTimerDelay(model.getDifficultyLevel().getDelayTime());
                    model.setGameState(GameState.ACTIVE);
                    break;
            }

        } else if (model.getGameState() == GameState.ACTIVE) {
            if (e.getKeyCode() == KeyEvent.VK_P) {
                model.setGameState(GameState.PAUSED);
                pauseGame();
                view.repaint();
            }

            Direction currentDirection = model.getCurrentDirection();

            switch (e.getKeyCode()) {
                case KeyEvent.VK_RIGHT: {
                    if (currentDirection != Direction.LEFT) {
                        currentDeltaRow = 0;
                        currentDeltaCol = 1;
                        model.setCurrentDirection(Direction.RIGHT);
                    }
                    break;
                }
                case KeyEvent.VK_LEFT: {
                    if (currentDirection != Direction.RIGHT) {
                        currentDeltaRow = 0;
                        currentDeltaCol = -1;
                        model.setCurrentDirection(Direction.LEFT);
                    }
                    break;
                }
                case KeyEvent.VK_UP: {
                    if (currentDirection != Direction.DOWN) {
                        currentDeltaRow = -1;
                        currentDeltaCol = 0;
                        model.setCurrentDirection(Direction.UP);
                    }
                    break;
                }
                case KeyEvent.VK_DOWN: {
                    if (currentDirection != Direction.UP) {
                        currentDeltaRow = 1;
                        currentDeltaCol = 0;
                        model.setCurrentDirection(Direction.DOWN);
                    }
                    break;
                }
            }
        } else if (model.getGameState() == GameState.PAUSED) {
            if (e.getKeyCode() == KeyEvent.VK_P) {
                model.setGameState(GameState.ACTIVE);
                pauseGame();
                view.repaint();
            }
        } else if (model.getGameState() == GameState.GAME_OVER) {
            if (e.getKeyCode() == KeyEvent.VK_SPACE) {
                model.setGameState(GameState.HOME);
                model.resetHome();
                model.setInitialDirection();
                currentDeltaRow = 0; // Reset the delta values
                currentDeltaCol = 1;
                timeRemaining = gameTime; // Reset the timeRemaining
                view.repaint();
            }
            if (e.getKeyCode() == KeyEvent.VK_ENTER) {
                model.setGameState(GameState.ACTIVE);
                model.reset();
                model.setInitialDirection();
                currentDeltaRow = 0; // Reset the delta values
                currentDeltaCol = 1;
                timeRemaining = gameTime; // Reset the timeRemaining
                view.repaint();

            }
        }
    }

    @Override
    public void keyReleased(KeyEvent arg0) {
    }

    @Override
    public void keyTyped(KeyEvent arg0) {
    }

    @Override
    public void actionPerformed(ActionEvent arg0) {
        // Called every 'delay' milliseconds
        if (model.getGameState() == GameState.ACTIVE) {
            model.moveSnake(currentDeltaRow, currentDeltaCol);
            view.repaint();

            if (model.getDifficultyLevel() == DifficultyLevel.TIMED && arg0.getSource() == timer) {
                timeRemaining--; // decrease by 1 second
                view.displayTimeRemaining(timeRemaining);
                if (timeRemaining <= 0) {
                    model.updateHighScore(); // Update the high score when time runs out
                    model.setGameState(GameState.GAME_OVER);
                    model.getHighScore();

                }
            }
        }
    }
}
