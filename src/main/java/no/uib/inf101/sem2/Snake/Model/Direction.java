package no.uib.inf101.sem2.snake.model;

//Direction is an enumeration that represents the possible directions a snake can move in the Snake game.
public enum Direction {

    UP(-1, 0),

    DOWN(1, 0),

    LEFT(0, -1),

    RIGHT(0, 1);

    private final int dRow;

    private final int dCol;

    /**
     * Constructs a new direction with the specified change in row and column.
     * 
     * @param dRow the change in row
     * @param dCol the change in column
     */
    Direction(int dRow, int dCol) {
        this.dRow = dRow;
        this.dCol = dCol;
    }

    public int getDRow() {
        return dRow;
    }

    public int getDCol() {
        return dCol;
    }

    /**
     * Returns the direction that corresponds to the specified change in row and
     * column.
     * 
     * @param dRow the change in row
     * @param dCol the change in column
     * @return the direction that corresponds to the specified change in row and
     *         column
     * @throws IllegalArgumentException if the specified delta values are not valid
     */
    public static Direction fromDelta(int dRow, int dCol) {
        if (dRow == 0 && dCol == 0) {
            return null;
        }
        for (Direction direction : values()) {
            if (direction.dRow == dRow && direction.dCol == dCol) {
                return direction;
            }
        }
        throw new IllegalArgumentException("Invalid delta values for direction: (" + dRow + ", " + dCol + ")");
    }
}
