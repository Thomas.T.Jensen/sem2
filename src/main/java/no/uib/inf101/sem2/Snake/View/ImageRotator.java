package no.uib.inf101.sem2.snake.view;

import java.awt.Graphics2D;
import java.awt.image.BufferedImage;

import no.uib.inf101.sem2.snake.model.Direction;

public class ImageRotator {

    /**
     * Rotates the given `BufferedImage` based on the specified `Direction`.
     * 
     * @param image     the `BufferedImage` to rotate
     * @param direction the `Direction` to rotate the image
     * @return the rotated `BufferedImage`
     */
    public static BufferedImage rotate(BufferedImage image, Direction direction) {
        double rotation;
        switch (direction) {
            case UP:
                rotation = Math.toRadians(-90);
                break;
            case DOWN:
                rotation = Math.toRadians(90);
                break;
            case LEFT:
                rotation = Math.toRadians(180);
                break;
            case RIGHT:
            default:
                rotation = 0;
                break;
        }
        // Source:https://stackoverflow.com/questions/37758061/rotate-a-buffered-image-in-java
        int w = image.getWidth();
        int h = image.getHeight();
        BufferedImage rotatedImage = new BufferedImage(w, h, image.getType());
        Graphics2D g2 = rotatedImage.createGraphics();
        g2.rotate(rotation, w / 2.0, h / 2.0);
        g2.drawImage(image, 0, 0, null);
        g2.dispose();

        return rotatedImage;
    }
}
