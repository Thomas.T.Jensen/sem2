package no.uib.inf101.sem2.snake.model;

//GameState is an enumeration that represents the state of a Snake game
public enum GameState {
    HOME,
    ACTIVE,
    PAUSED,
    MULTI_PLAYER,
    GAME_OVER
}
