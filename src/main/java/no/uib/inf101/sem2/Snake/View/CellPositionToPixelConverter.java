package no.uib.inf101.sem2.snake.view;

import java.awt.geom.Rectangle2D;

import no.uib.inf101.sem2.grid.CellPosition;
import no.uib.inf101.sem2.grid.GridDimension;

//Hele koden er direkte kopiert fra Lab 4 CellPositionToPixelConverter

public class CellPositionToPixelConverter {
    // Create instance variables
    private Rectangle2D box;
    private GridDimension gd;
    private double margin;

    public CellPositionToPixelConverter(Rectangle2D box, GridDimension gd, double margin) {
        this.box = box;
        this.gd = gd;
        this.margin = margin;
    }

    public Rectangle2D getBoundsForCell(CellPosition pos) {
        // Get the dimensions of the grid box
        double Boxwidth = box.getWidth();
        double BoxHeight = box.getHeight();
        double BoxY = box.getY();
        double BoxX = box.getX();
        // Get the row and column of the given cell position
        double cpCol = pos.col();
        double cpRow = pos.row();
        // Get the number of columns and rows in the grid
        double gdCols = gd.cols();
        double GdRows = gd.rows();

        // Calculate the width and height of each cell
        double cellWidth = (Boxwidth - (margin * (gdCols + 1))) / gdCols;
        double cellHeight = (BoxHeight - (margin * (GdRows + 1))) / GdRows;

        // Calculate the x and y coordinates of the cell
        double CellX = (BoxX + (margin * (cpCol + 1)) + (cellWidth * cpCol));
        double CellY = (BoxY + (margin * (cpRow + 1)) + (cellHeight * cpRow));

        // Create a new Rectangle2D object representing the bounds of the cell
        Rectangle2D cell = new Rectangle2D.Double(CellX, CellY, cellWidth, cellHeight);

        // Return the Rectangle2D object
        return cell;

    }

}
