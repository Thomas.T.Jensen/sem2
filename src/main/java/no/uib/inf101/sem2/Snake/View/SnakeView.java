package no.uib.inf101.sem2.snake.view;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.imageio.ImageIO;
import javax.swing.JPanel;

import no.uib.inf101.sem2.grid.CellPosition;
import no.uib.inf101.sem2.grid.GridCell;
import no.uib.inf101.sem2.snake.controller.DifficultyLevel;
import no.uib.inf101.sem2.snake.model.Direction;
import no.uib.inf101.sem2.snake.model.GameState;

public class SnakeView extends JPanel {

    private BufferedImage appleImage;
    private BufferedImage snakeHeadRight;
    private BufferedImage snakeBody;
    private BufferedImage homeImage;
    private BufferedImage gameOver;
    private int timeRemaining;
    private List<Direction> bodyDirections;
    private Map<Direction, BufferedImage> rotatedSnakeHeads;
    private Map<Direction, BufferedImage> rotatedSnakeBodies;
    private ViewableSnakeModel model;
    private final double INNER_MARGIN;
    private final double OUTER_MARGIN;
    private static final Color MARGINCOLOR = Color.LIGHT_GRAY;
    private ColorTheme colorTheme;

    public SnakeView(ViewableSnakeModel model) {
        this.model = model;
        this.setFocusable(true);
        this.setPreferredSize(new Dimension(750, 750));
        colorTheme = new DefaultColorTheme();
        this.setBackground(colorTheme.getBackgroundColor());
        this.setDoubleBuffered(true);
        INNER_MARGIN = 2.0;
        OUTER_MARGIN = 20.0;

        loadImages();
        bodyDirections = new ArrayList<>();
        rotatedSnakeHeads = new HashMap<>();
        rotatedSnakeBodies = new HashMap<>();
        for (Direction direction : Direction.values()) {
            rotatedSnakeHeads.put(direction, ImageRotator.rotate(snakeHeadRight, direction));
            rotatedSnakeBodies.put(direction, ImageRotator.rotate(snakeBody, direction));
        }
    }

    /**
     * Loads various images used in the game including the apple, snake head and
     * body,
     * home screen, and game over screen.
     */
    private void loadImages() {
        appleImage = loadImage("/no/uib/inf101/sem2/snake/view/Images/apple.png");
        snakeHeadRight = loadImage("/no/uib/inf101/sem2/snake/view/Images/snakeHeadRight.png");
        snakeBody = loadImage("/no/uib/inf101/sem2/snake/view/Images/snakeBody.png");
        homeImage = loadImage("/no/uib/inf101/sem2/snake/view/Images/HomeImage.png"); // I have used Paint to add some
                                                                                      // information on the image
        gameOver = loadImage("/no/uib/inf101/sem2/snake/view/Images/gameOverScreen.png"); // I have used Paint to add
                                                                                          // some information on the
                                                                                          // image
    }

    // Source: https://docs.oracle.com/javase/tutorial/2d/images/loadimage.html
    /**
     * Loads an image from a file path.
     * 
     * @param imagePath The file path of the image to be loaded.
     * @return The BufferedImage object containing the loaded image.
     */
    private BufferedImage loadImage(String imagePath) {
        BufferedImage image = null;
        try {
            image = ImageIO.read(getClass().getResourceAsStream(imagePath));
        } catch (IOException e) {
            e.printStackTrace();
        }
        return image;
    }

    private void drawHomeScreen(Graphics2D g2) {
        g2.drawImage(homeImage, 0, 0, getWidth(), getHeight(), null);
    }

    /**
     * Draws the cells of the game board.
     * 
     * @param g2         The Graphics2D object to be used for drawing.
     * @param cells      An Iterable object containing the GridCells to be drawn.
     * @param converter  A CellPositionToPixelConverter object used for mapping cell
     *                   positions to pixel coordinates.
     * @param colorTheme The ColorTheme object containing the color scheme for the
     *                   game.
     */
    private void drawCells(Graphics2D g2, Iterable<GridCell<Character>> cells, CellPositionToPixelConverter converter,
            ColorTheme colorTheme) {

        // Loop through each cell in the Iterable object
        for (GridCell<Character> cell : cells) {
            // Get the bounds for the current cell using the CellPositionToPixelConverter
            Rectangle2D box = converter.getBoundsForCell(cell.pos());
            // Get the color for the current cell from the ColorTheme
            Color color = new Color(102, 255, 102);
            // Set the Graphics2D color to the color of the cell
            g2.setColor(color);
            // Fill the rectangle of the cell with the color
            g2.fill(box);
        }
    }

    private void updateBodyDirections() {
        Iterable<GridCell<Character>> snakeCells = model.getSnake();
        List<CellPosition> cellPositions = new ArrayList<>();
        for (GridCell<Character> cell : snakeCells) {
            cellPositions.add(cell.pos());
        }
        bodyDirections.clear();
        for (int i = 1; i < cellPositions.size(); i++) {
            CellPosition current = cellPositions.get(i);
            CellPosition previous = cellPositions.get(i - 1);
            bodyDirections.add(Direction.fromDelta(current.row() - previous.row(), current.col() - previous.col()));
        }
    }

    /**
     * 
     * Returns the cached rotated image of the snake head in the current direction.
     * 
     * @return The cached rotated image of the snake head
     */
    private BufferedImage getCachedRotatedSnakeHead() {
        return rotatedSnakeHeads.get(model.getCurrentDirection());
    }

    /**
     * 
     * Draws the snake on the game board using the given Graphics object and
     * CellPositionToPixelConverter.
     * 
     * @param g         The Graphics object to use for drawing
     * @param converter The CellPositionToPixelConverter to convert cell positions
     *                  to pixel coordinates
     */
    private void drawSnake(Graphics g, CellPositionToPixelConverter converter) {
        updateBodyDirections();
        Iterable<GridCell<Character>> snakeCells = model.getSnake();
        int i = 0;
        for (GridCell<Character> cell : snakeCells) {
            CellPosition pos = cell.pos();
            Rectangle2D box = converter.getBoundsForCell(pos);
            int x = (int) box.getX();
            int y = (int) box.getY();
            int width = (int) box.getWidth();
            int height = (int) box.getHeight();

            if (i == 0) { // Draw the snake's head
                BufferedImage cachedRotatedHead = getCachedRotatedSnakeHead();
                g.drawImage(cachedRotatedHead, x, y, width, height, null);
            } else { // Draw the snake's body
                Direction bodyDirection = bodyDirections.get(i - 1);
                BufferedImage cachedRotatedBody = rotatedSnakeBodies.get(bodyDirection);
                g.drawImage(cachedRotatedBody, x, y, width, height, null);
            }
            i++;
        }
    }

    /**
     * Draws the apple on the game board.
     * 
     * @param g         the graphics object used for drawing
     * @param converter the converter used for mapping cell positions to pixel
     *                  coordinates
     */
    private void drawApple(Graphics g, CellPositionToPixelConverter converter) {
        Iterable<GridCell<Character>> appleCells = model.appleTile();
        for (GridCell<Character> cell : appleCells) {
            CellPosition pos = cell.pos();
            Rectangle2D box = converter.getBoundsForCell(pos);
            int x = (int) box.getX();
            int y = (int) box.getY();
            int width = (int) box.getWidth();
            int height = (int) box.getHeight();
            g.drawImage(appleImage, x, y, width, height, null);
        }
    }

    /**
     * Draws the game board and its elements.
     * 
     * @param g2 the graphics object used for drawing
     */
    private void drawGame(Graphics2D g2) {
        // Calculate the bounds of the game board
        double x = OUTER_MARGIN;
        double y = OUTER_MARGIN;
        double width = this.getWidth() - 2 * OUTER_MARGIN;
        double height = this.getHeight() - 2 * OUTER_MARGIN;

        // Draw the margins around the board
        g2.setColor(MARGINCOLOR);
        Rectangle2D rectangle = new Rectangle2D.Double(x, y, width, height);
        g2.fill(rectangle);

        // Create a converter to map cell positions to pixel coordinates

        CellPositionToPixelConverter converter = new CellPositionToPixelConverter(rectangle, model.getDimension(),
                INNER_MARGIN);

        // Draw the cells on the board
        drawCells(g2, this.model.gameTile(), converter, this.colorTheme);

        drawSnake(g2, converter);
        // Draw the snake cells
        drawApple(g2, converter);
    }

    /**
     * Displays information about the game state, such as the score and the current
     * direction of the snake.
     * 
     * @param g2 the Graphics2D object used for painting
     */
    public void activeInfo(Graphics2D g2) {
        // Display the "Press p to pause" message
        if (model.getGameState() == GameState.ACTIVE) {
            g2.setColor(Color.BLACK);
            g2.setFont(new Font("Arial", Font.PLAIN, 16));
            g2.drawString("Press p to pause", (int) OUTER_MARGIN, getHeight() - 5);
        }
        if (model.getGameState() == GameState.PAUSED) {
            g2.setColor(Color.BLACK);
            g2.setFont(new Font("Arial", Font.PLAIN, 16));
            g2.drawString("Press p to un-pause", (int) OUTER_MARGIN, getHeight() - 5);
        }

        // Display the "Score:" message
        g2.setFont(new Font("Arial", Font.PLAIN, 16));
        g2.drawString("Score: " + model.getScore(), (int) (getWidth() - OUTER_MARGIN - 100), getHeight() - 5);

        // Display the "High Score:" message
        g2.setFont(new Font("Arial", Font.PLAIN, 16));
        g2.drawString("High Score: " + model.getHighScore(), (int) (getWidth() / 2 - 50), getHeight() - 5);

        g2.setFont(new Font("Arial", Font.PLAIN, 16));
        String directionMessage = "Direction " + model.getCurrentDirection();
        int directionMessageWidth = g2.getFontMetrics().stringWidth(directionMessage);
        g2.drawString(directionMessage, (getWidth() - directionMessageWidth) / 2,
                (getHeight()) / 6 - 108);
    }

    /**
     * Draws the game over screen with the score and high score.
     * 
     * @param g2 the Graphics2D object used to draw the graphics.
     */
    public void gameOver(Graphics2D g2) {
        g2.drawImage(gameOver, 0, 0, getWidth(), getHeight(), null);

        g2.setColor(Color.BLACK);
        g2.setFont(new Font("Jokerman", Font.PLAIN, 20));
        String scoreMessage = "Score: " + model.getScore();
        int scoreWidth = g2.getFontMetrics().stringWidth(scoreMessage);
        int scoreHeight = g2.getFontMetrics().getHeight();
        g2.drawString(scoreMessage, (getWidth() - scoreWidth) / 2, (getHeight() / 2) + (scoreHeight / 2 - 140));

        // Display the "High Score:" message
        g2.setColor(Color.BLACK);
        g2.setFont(new Font("Jokerman", Font.PLAIN, 20));
        String highScoreMessage = "High Score: " + model.getHighScore();
        int highScoreWidth = g2.getFontMetrics().stringWidth(highScoreMessage);
        int highScoreHeight = g2.getFontMetrics().getHeight();
        g2.drawString(highScoreMessage, (getWidth() - highScoreWidth) / 2,
                (getHeight() / 2 + 40) + (highScoreHeight * 2 - 190));

    }

    /**
     * Displays the remaining time for timed games.
     * 
     * @param timeRemaining the remaining time for timed games.
     */
    public void displayTimeRemaining(int timeRemaining) {
        this.timeRemaining = timeRemaining;
    }

    @Override
    public void paintComponent(Graphics g) {
        // Call the parent paintComponent method to ensure proper painting of the
        // component
        super.paintComponent(g);
        // Cast the Graphics object to Graphics2D to enable advanced graphics operations
        Graphics2D g2 = (Graphics2D) g;

        if (model.getGameState() == GameState.HOME) {
            drawHomeScreen(g2);
        } else if (model.getGameState() == GameState.ACTIVE) {
            drawGame(g2);
            activeInfo(g2);
            if (model.getDifficultyLevel() == DifficultyLevel.TIMED) {
                g2.setColor(Color.BLACK);
                g2.setFont(new Font("Arial", Font.PLAIN, 16));
                g2.drawString("Time Remaining: " + timeRemaining, (int) OUTER_MARGIN, 15);
            }

        } else if (model.getGameState() == GameState.PAUSED) {
            drawGame(g2);
            activeInfo(g2);
            if (model.getDifficultyLevel() == DifficultyLevel.TIMED) {
                g2.setColor(Color.BLACK);
                g2.setFont(new Font("Arial", Font.PLAIN, 16));
                g2.drawString("Time Remaining: " + timeRemaining, (int) OUTER_MARGIN, 15);
            }
            g.setColor(new Color(128, 128, 128, 128)); // grayish color with transparency
            g.fillRect(0, 0, getWidth(), getHeight());
        } else if (model.getGameState() == GameState.GAME_OVER) {
            gameOver(g2);
        }

    }

}