package no.uib.inf101.sem2.snake.view;

import java.awt.Color;

public interface ColorTheme {

    /**
     * Returns the background color for the GUI component.
     * 
     * @return the background color
     */
    public Color getBackgroundColor();

}
