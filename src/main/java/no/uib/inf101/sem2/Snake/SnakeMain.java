
package no.uib.inf101.sem2.snake;

import javax.swing.JFrame;

import no.uib.inf101.sem2.snake.controller.SnakeController;
import no.uib.inf101.sem2.snake.model.SnakeBoard;
import no.uib.inf101.sem2.snake.model.SnakeModel;
import no.uib.inf101.sem2.snake.view.SnakeView;

// The entire game and code was initially inspired by a very small and basic snakeGame turtorial on youtube: //https://www.youtube.com/watch?v=bI6e6qjJ8JQ&t=1466s
// It was also inspired by Lab Snake from Inf100 and Tetris from Inf101

// SnakeMain is the main class that sets up the Snake game by creating the game
// board, initializing the model, view and controller, and displaying the game
// window.
public class SnakeMain {

    public static final String WINDOW_TITLE = "INF101 Snake";

    public static void main(String[] args) {
        // create a SnakeBoard with 20 rows and 20 columns
        SnakeBoard board = new SnakeBoard(20, 20);
        SnakeModel model = new SnakeModel(board);
        SnakeView view = new SnakeView(model);
        new SnakeController(model, view);

        // The JFrame is the "root" application window.
        // We here set som properties of the main window,
        // and tell it to display our tetrisView
        JFrame frame = new JFrame(WINDOW_TITLE);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        // Here we set which component to view in our window
        frame.setContentPane(view);

        // Call these methods to actually display the window
        frame.pack();
        frame.setVisible(true);
    }
}
