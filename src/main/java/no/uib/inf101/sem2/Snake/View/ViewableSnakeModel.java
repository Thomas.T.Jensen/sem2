package no.uib.inf101.sem2.snake.view;

import no.uib.inf101.sem2.grid.GridCell;
import no.uib.inf101.sem2.grid.GridDimension;
import no.uib.inf101.sem2.snake.controller.DifficultyLevel;
import no.uib.inf101.sem2.snake.model.Direction;
import no.uib.inf101.sem2.snake.model.GameState;

public interface ViewableSnakeModel {

    /**
     * Return the dimension of the grid.
     *
     * @return the grid dimension.
     */
    public GridDimension getDimension();

    /**
     * Returns an iterable of grid cells representing the game tiles.
     *
     * @return an iterable of game tiles.
     */
    public Iterable<GridCell<Character>> gameTile();

    /**
     * Returns an iterable of grid cells representing the apple tiles.
     *
     * @return an iterable of apple tiles.
     */
    public Iterable<GridCell<Character>> appleTile();

    /**
     * Return the current score of the game.
     *
     * @return the score.
     */
    public int getScore();

    /**
     * Returns an iterable of grid cells representing the snake.
     *
     * @return an iterable of snake tiles.
     */
    public Iterable<GridCell<Character>> getSnake();

    /**
     * Return the current state of the game.
     *
     * @return the game state.
     */

    public GameState getGameState();

    /**
     * Returns the current high score.
     *
     * @return the high score.
     */
    public int getHighScore();

    /**
     * Sets the initial direction of the snake.
     */
    public void setInitialDirection();

    /**
     * Returns the current direction of the snake.
     *
     * @return the current direction.
     */
    public Direction getCurrentDirection();

    /**
     * Sets the current direction of the snake.
     *
     * @param direction the new direction.
     */
    public void setCurrentDirection(Direction direction);

    /**
     * Returns the difficulty level of the game.
     * 
     * @return the difficulty level of the game.
     */
    public DifficultyLevel getDifficultyLevel();

}
