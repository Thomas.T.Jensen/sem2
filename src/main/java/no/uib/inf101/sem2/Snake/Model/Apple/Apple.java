package no.uib.inf101.sem2.snake.model.Apple;

import no.uib.inf101.sem2.grid.CellPosition;
import no.uib.inf101.sem2.grid.GridCell;

// Apple is a class that represents an apple in the Snake game.
public class Apple {
    /**
     * The symbol that represents the apple on the board.
     */
    private final char symbol;

    /**
     * The cell position of the apple on the board.
     */
    private final CellPosition pos;

    /**
     * Constructs a new Apple with the specified symbol and position.
     * 
     * @param symbol the symbol that represents the apple on the board
     * @param pos    the cell position of the apple on the board
     */
    public Apple(char symbol, CellPosition pos) {
        this.symbol = symbol;
        this.pos = pos;
    }

    /**
     * Returns a `GridCell` representation of the apple, suitable for use with the
     * `Grid` class.
     * 
     * @return a `GridCell` representation of the apple
     */
    public GridCell<Character> getGridCell() {
        return new GridCell<>(pos, symbol);
    }

    /**
     * Returns the cell position of the apple on the board.
     * 
     * @return the cell position of the apple on the board
     */
    public CellPosition getPosition() {
        return pos;
    }
}