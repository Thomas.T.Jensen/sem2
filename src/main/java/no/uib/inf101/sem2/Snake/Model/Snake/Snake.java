package no.uib.inf101.sem2.snake.model.Snake;

import java.util.ArrayList;
import java.util.Iterator;

import no.uib.inf101.sem2.grid.CellPosition;
import no.uib.inf101.sem2.grid.GridCell;

//Snake is a class that represents a snake in the Snake game. It implements the `Iterable` interface to allow easy iteration over the cells that make up the snake's body.
public class Snake implements Iterable<GridCell<Character>> {
    /**
     * The symbol that represents the body of the snake.
     */
    private final char bodySymbol;

    /**
     * The symbol that represents the head of the snake.
     */
    private final char headSymbol;

    /**
     * The list of cell positions that make up the body of the snake. The head of
     * the snake is at the first position in the list.
     */
    private final ArrayList<CellPosition> body;

    public Snake(char bodySymbol, char headSymbol, ArrayList<CellPosition> newBody) {
        this.bodySymbol = bodySymbol;
        this.headSymbol = headSymbol;
        this.body = newBody;

    }

    public ArrayList<CellPosition> getBody() {
        return body;
    }

    public CellPosition getHeadPos() {
        return body.get(0);
    }

    /**
     * 
     * Creates a new snake object with the specified parameters.
     * 
     * @param bodySymbol the symbol that represents the body of the snake
     * @param headSymbol the symbol that represents the head of the snake
     * @param headPos    the position of the snake's head
     * @param snakeSize  the size of the snake's body
     * @return a new Snake object
     */
    public static Snake createSnake(char bodySymbol, char headSymbol, CellPosition headPos, int snakeSize) {
        ArrayList<CellPosition> newBody = new ArrayList<>();
        for (int i = 0; i < snakeSize; i++) {
            newBody.add(new CellPosition(headPos.row(), headPos.col() - i));
        }
        return new Snake(bodySymbol, headSymbol, newBody);
    }

    /**
     * 
     * Creates a new snake object with the specified parameters.
     * 
     * @param bodySymbol the symbol that represents the body of the snake
     * @param headSymbol the symbol that represents the head of the snake
     * @param headPos    the position of the snake's head
     * @param snakeSize  the size of the snake's body
     * @return a new Snake object
     */
    public Snake withHeadAt(CellPosition headPos) {
        ArrayList<CellPosition> newBody = new ArrayList<>(body);
        newBody.add(0, headPos);
        newBody.remove(newBody.size() - 1);
        return new Snake(bodySymbol, headSymbol, newBody);
    }

    /**
     * 
     * Returns a new Snake object shifted by the specified delta values.
     * 
     * @param deltaRow the change in row position
     * @param deltaCol the change in column position
     * @return a new Snake object
     */
    public Snake shiftedBy(int deltaRow, int deltaCol) {
        CellPosition newHeadPos = new CellPosition(getHeadPos().row() + deltaRow, getHeadPos().col() + deltaCol);
        return withHeadAt(newHeadPos);
    }

    @Override
    public Iterator<GridCell<Character>> iterator() {
        ArrayList<GridCell<Character>> cellList = new ArrayList<>();
        for (int i = 0; i < body.size(); i++) {
            CellPosition pos = body.get(i);
            char symbol = i == 0 ? headSymbol : bodySymbol;
            GridCell<Character> gridCell = new GridCell<Character>(pos, symbol);
            cellList.add(gridCell);
        }
        return cellList.iterator();
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + bodySymbol;
        result = prime * result + headSymbol;
        result = prime * result + ((body == null) ? 0 : body.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Snake other = (Snake) obj;
        if (bodySymbol != other.bodySymbol)
            return false;
        if (headSymbol != other.headSymbol)
            return false;
        if (body == null) {
            if (other.body != null)
                return false;
        } else if (!body.equals(other.body))
            return false;
        return true;
    }

}
