package no.uib.inf101.sem2.snake.model;

import java.util.ArrayList;

import no.uib.inf101.sem2.grid.CellPosition;
import no.uib.inf101.sem2.grid.GridCell;
import no.uib.inf101.sem2.grid.GridDimension;
import no.uib.inf101.sem2.snake.controller.ControllableSnakeModel;
import no.uib.inf101.sem2.snake.controller.DifficultyLevel;
import no.uib.inf101.sem2.snake.model.Apple.Apple;
import no.uib.inf101.sem2.snake.model.Apple.RandomApple;
import no.uib.inf101.sem2.snake.model.Snake.Snake;
import no.uib.inf101.sem2.snake.view.ViewableSnakeModel;

public class SnakeModel implements ViewableSnakeModel, ControllableSnakeModel {
    private SnakeBoard board;
    private Snake snake;
    private GameState gameState = GameState.HOME;
    private Apple apple;
    private RandomApple appleFactory;
    private int score;
    private DifficultyLevel difficultyLevel = DifficultyLevel.EASY;
    private int highScore;
    private Direction currentDirection = Direction.RIGHT;

    public SnakeModel(SnakeBoard board) {
        this.board = board;
        this.snake = Snake.createSnake('S', 'H', new CellPosition(5, 5), 3);
        this.appleFactory = new RandomApple(board.rows(), board.cols());
        this.apple = appleFactory.getNext(snake);
        this.score = 0;
        this.highScore = 0;

    }

    @Override
    public GridDimension getDimension() {
        return board;
    }

    @Override
    public Iterable<GridCell<Character>> gameTile() {
        return board;
    }

    @Override
    public Iterable<GridCell<Character>> appleTile() {

        ArrayList<GridCell<Character>> appleList = new ArrayList<>();
        appleList.add(apple.getGridCell());
        return appleList;
    }

    @Override
    public Iterable<GridCell<Character>> getSnake() {
        return snake;
    }

    public Boolean moveSnake(int deltaRow, int deltaCol) {
        Snake newSnake = this.snake.shiftedBy(deltaRow, deltaCol);

        if (isLegalMove(newSnake)) {
            this.snake = newSnake;
            eatApple();
            return true;
        } else {
            gameState = GameState.GAME_OVER;
            updateHighScore(); // Update the high score when the game is over
            return false;
        }
    }

    /**
     * Checks if the specified move is legal for the snake, i.e., if the snake will
     * not go out of bounds or collide with its own body.
     * 
     * @param snake the snake whose move is being checked
     * @return true if the move is legal, false otherwise
     */
    private Boolean isLegalMove(Snake snake) {
        CellPosition headPos = snake.getHeadPos();
        int boardRows = this.board.rows();
        int boardCols = this.board.cols();

        if (headPos.col() < 0 || headPos.col() >= boardCols ||
                headPos.row() < 0 || headPos.row() >= boardRows) {
            return false;
        }

        // Check for collision with the snake's body (starting from index 1)
        for (int i = 1; i < snake.getBody().size(); i++) {
            if (headPos.equals(snake.getBody().get(i))) {
                return false;
            }
        }

        return true;
    }

    /**
     * The eatApple method checks if the snake's head position is equal to the
     * apple's position. If they are the same,
     * the growSnake method is called to increase the size of the snake, and the
     * current apple is eaten.
     */
    private void eatApple() {
        if (snake.getHeadPos().equals(apple.getPosition())) {
            growSnake();
            apple = appleFactory.getNext(snake);
            this.score += 10;
        }

    }

    /**
     * The growSnake method increases the size of the snake by adding a new cell to
     * the end of the snake's body.
     * It creates a new ArrayList with the same elements as the current snake's
     * body, then gets the position of the
     * last cell in the list and adds a new cell with the same position to the end
     * of the list. The snake variable is
     * then updated to a new Snake instance with the new body.
     */
    private void growSnake() {
        ArrayList<CellPosition> newBody = new ArrayList<>(snake.getBody());
        CellPosition tail = newBody.get(newBody.size() - 1);
        newBody.add(tail);
        snake = new Snake('S', 'H', newBody);
    }

    @Override
    public GameState getGameState() {
        return gameState;
    }

    @Override
    public int getScore() {
        return this.score;
    }

    @Override
    public void updateHighScore() {
        if (this.score > this.highScore) {
            this.highScore = this.score;
        }
    }

    @Override
    public int getHighScore() {
        return this.highScore;
    }

    @Override
    public void setGameState(GameState state) {
        this.gameState = state;
    }

    /**
     * Resets the game and changes gamestate to Home
     */
    public void resetHome() {
        // Reset the snake position and direction
        this.snake = Snake.createSnake('S', 'H', new CellPosition(2, 3), 3);
        // Reset the apple position
        this.apple = appleFactory.getNext(snake);

        // Reset the score
        this.score = 0;

        // Set the game state to HOME
        this.gameState = GameState.HOME;
    }

    /**
     * Resets the game and changes gamestate to Home
     */
    public void reset() {
        this.snake = Snake.createSnake('S', 'H', new CellPosition(2, 3), 3);
        // Reset the apple position
        this.apple = appleFactory.getNext(snake);

        // Reset the score
        this.score = 0;

        // Set the game state to ACTIVE
        this.gameState = GameState.ACTIVE;
    }

    @Override
    public void setCurrentDirection(Direction direction) {
        this.currentDirection = direction;
    }

    @Override
    public Direction getCurrentDirection() {
        return this.currentDirection;
    }

    @Override
    public void setInitialDirection() {
        this.currentDirection = Direction.RIGHT;
    }

    @Override
    public DifficultyLevel getDifficultyLevel() {
        return difficultyLevel;
    }

    @Override
    public void setDifficultyLevel(DifficultyLevel difficultyLevel) {
        this.difficultyLevel = difficultyLevel;
    }

}
