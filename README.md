# Mitt program
Youtube video, beskrivelse av spillet: https://www.youtube.com/watch?v=ifN-E0hYYyA

Snake Game

Dette er en enkel implementasjon av et snake-spill i Java, hvor spilleren kontrollerer en slange som beveger seg rundt på skjermen og prøver å spise mat samtidig som den unngår kollisjon med sin egen kropp og skjermens kanter. Målet er å få slangen til å vokse så lenge som mulig ved å spise mat.
Features

 Forskjellige vanskelighetsgrader: Spillet tilbyr flere vanskelighetsgrader for å passe til dine preferanser. Disse inkluderer:
    Enkel (easy)
    Medium  (medium)
    Vanskelig (hard)
    Tidsbestemt (timed)

Poengtavle: Spillet holder oversikt over din nåværende poengsum, som øker når slangen spiser mat.

Høyeste poengsum: Spillet lagrer og viser den høyeste poengsummen oppnådd.

Kontroller

Her er de grunnleggende kontrollene for snake-spillet:

    Piltaster (↑, ↓, ←, →): Kontroller bevegelsen til slangen.
    'P': Pause og fortsett spillet.
    'Mellomromstasten': Gå til startsiden etter at spillet er over.
    '1', '2', '3', '4': Velg vanskelighetsgrad og start spillet (tilgjengelig på startsiden).


Difficulty Levels

    Enkel: Slangen beveger seg langsommere, noe som gjør spillet enklere å kontrollere.
    Medium: Slangen beveger seg i moderat tempo, og gir en balansert spillopplevelse.
    Vanskelig: Slangen beveger seg i høy hastighet, noe som gjør spillet mer utfordrende.
    Tidsbestemt: Slangen beveger seg i moderat tempo, men spillet er tidsbegrenset. Du har 400 sekunder på deg til å oppnå den høyeste poengsummen mulig.


Hvordan du spiller

  Bruk piltastene for å navigere slangen rundt på skjermen.
    Spis maten som vises på skjermen for å få slangen til å vokse og øke poengsummen din.
    Unngå kollisjoner med slangens kropp eller skjermens kanter, da dette vil resultere i spill over.
    I tidsbestemt modus, prøv å oppnå den høyeste poengsummen innenfor den 400 sekunders tidsbegrensningen.


